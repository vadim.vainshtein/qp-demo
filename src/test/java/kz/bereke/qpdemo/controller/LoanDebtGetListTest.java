package kz.bereke.qpdemo.controller;

import kz.bereke.esb.qp.test.BasicControllerTest;
import kz.bereke.qpdemo.model.loandebtgetlist.bcon.LoanDebtGetListBconRequest;
import kz.bereke.qpdemo.model.loandebtgetlist.bcon.LoanDebtGetListBconResponse;
import kz.bereke.qpdemo.model.loandebtgetlist.qp.LoanDebtGetListQpRequest;
import kz.bereke.qpdemo.model.loandebtgetlist.qp.LoanDebtGetListQpResponse;
import org.junit.jupiter.api.Test;

class LoanDebtGetListTest extends BasicControllerTest {

  @Test
  void loanDebtGetList() throws Exception {
    testBconQpRequest(
        "loandebtgetlist",
        LoanDebtGetListBconRequest.class,
        LoanDebtGetListBconResponse.class,
        LoanDebtGetListQpRequest.class,
        LoanDebtGetListQpResponse.class,
        "/qp/loandebtgetlist");
  }
}