package kz.bereke.qpdemo.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import kz.bereke.esb.general.annotation.ELKLogController;
import kz.bereke.esb.qp.general.service.QpHandlerProxy;
import kz.bereke.qpdemo.model.loandebtgetlist.bcon.LoanDebtGetListBconRequest;
import kz.bereke.qpdemo.model.loandebtgetlist.bcon.LoanDebtGetListBconResponse;
import kz.bereke.qpdemo.service.LoanDebtGetListRequestHandler;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("qp")
@RequiredArgsConstructor
public class QpController {

  private final QpHandlerProxy qpHandlerProxy;

  @ELKLogController(eventReceiver = "QPRAGMA")
  @PostMapping(value = "loandebtgetlist", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
  public ResponseEntity<LoanDebtGetListBconResponse> loanDebtGetList(
      @RequestBody LoanDebtGetListBconRequest request) {
    var response = qpHandlerProxy.getHandler(LoanDebtGetListRequestHandler.class).handle(request);
    return ResponseEntity.ok(response);
  }
}
