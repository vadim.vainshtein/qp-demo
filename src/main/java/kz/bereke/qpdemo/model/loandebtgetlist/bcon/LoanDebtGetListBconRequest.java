package kz.bereke.qpdemo.model.loandebtgetlist.bcon;

import io.swagger.v3.oas.annotations.media.Schema;
import java.util.List;
import kz.bereke.esb.general.model.basic.request.BasicRequestDTO;
import kz.bereke.qpdemo.model.loandebtgetlist.qp.LoanDebtGetListQpRequest.PersonLoanDebtListRequest;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
public class LoanDebtGetListBconRequest extends BasicRequestDTO {

  private RqParms rqParms;

  @Data
  @Schema(name = "LoanDebtGetListRqParms")
  public static class RqParms {

    @Schema(example = "00")
    private String filialCode;
    private List<PersonLoanDebtListRequest> personLoanDebtListRequests;
  }
}
