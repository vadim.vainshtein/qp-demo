package kz.bereke.qpdemo.model.loandebtgetlist.qp;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_EMPTY;
import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import java.util.List;
import kz.bereke.esb.qp.general.model.basic.BasicQpResponseDTO;
import kz.bereke.esb.qp.general.model.basic.QpResponseData;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class LoanDebtGetListQpResponse extends
    BasicQpResponseDTO<LoanDebtGetListQpResponse.ResponseData> {

  @Data
  public static class ResponseData implements QpResponseData {

    @JacksonXmlProperty(localName = "PersonLoanDebtListResponse")
    private PersonLoanDebtListResponse personLoanDebtListResponse;
  }

  @Data
  @JsonInclude(NON_EMPTY)
  public static class PersonLoanDebtListResponse {

    @JacksonXmlProperty(localName = "PersonLoanDebtList")
    private PersonLoanDebtList personLoanDebtList;

    @JacksonXmlProperty(localName = "CreditCardList")
    private CreditCardList creditCardList;
  }

  @Data
  @JsonInclude(NON_EMPTY)
  public static class PersonLoanDebtList {

    @JacksonXmlProperty(localName = "gid", isAttribute = true)
    @Schema(example = "1002")
    private String gid;

    @JacksonXmlProperty(localName = "CardNumber")
    @Schema(example = "")
    private String cardNumber;

    @JacksonXmlProperty(localName = "FIO")
    @Schema(example = "СЕРДЮК АНАСТАСИЯ СЕРГЕЕВНА")
    private String fio;

    @JacksonXmlProperty(localName = "BankCommission")
    @Schema(example = "0.0")
    private String bankCommission;

    @JacksonXmlProperty(localName = "TypeRequest")
    @Schema(example = "1")
    private String typeRequest;

    @JacksonXmlProperty(localName = "TypePayment")
    @Schema(example = "cash")
    private String typePayment;

    @JacksonXmlElementWrapper(localName = "LoanDebtList")
    @JacksonXmlProperty(localName = "LoanDebt")
    private List<LoanDebt> loanDebtList;
  }

  @Data
  @JsonInclude(NON_EMPTY)
  public static class LoanDebt {

    @JacksonXmlProperty(localName = "gid", isAttribute = true)
    @Schema(example = "1")
    private String gid;

    @JacksonXmlProperty(localName = "CreditProgramName")
    @Schema(example = "")
    private CreditProgramName creditProgramName;

    @JacksonXmlProperty(localName = "CreditContTypeCat")
    @Schema(example = "1")
    private String creditContTypeCat;

    @JacksonXmlProperty(localName = "LoanContract")
    @Schema(example = "21-005355-00-ДБЗ")
    private String loanContract;

    @JacksonXmlProperty(localName = "OpenDate")
    @Schema(example = "2021-01-14")
    private String openDate;

    @JacksonXmlProperty(localName = "BalAccount")
    @Schema(example = "0.52")
    private String balAccount;

    @JacksonXmlProperty(localName = "FirstDebt")
    @Schema(example = "8931285.24")
    private String firstDebt;

    @JacksonXmlProperty(localName = "RewardAmount")
    @Schema(example = "105245.79")
    private String rewardAmount;

    @JacksonXmlProperty(localName = "OverdueFirstDebt")
    @Schema(example = "0.0")
    private String overdueFirstDebt;

    @JacksonXmlProperty(localName = "OverdueRewardAmount")
    @Schema(example = "0.0")
    private String overdueRewardAmount;

    @JacksonXmlProperty(localName = "Peny")
    @Schema(example = "0.0")
    private String peny;

    @JacksonXmlProperty(localName = "TotalAmount")
    @Schema(example = "105245.79")
    private String totalAmount;

    @JacksonXmlProperty(localName = "LoanCurrency")
    @Schema(example = "398")
    private String loanCurrency;

    @JacksonXmlProperty(localName = "ClientAccount")
    @Schema(example = "KZ04914001417KZ04P9T")
    private String clientAccount;

    @JacksonXmlProperty(localName = "CifAccount")
    @Schema(example = "KZ78914002204KZ00SWK")
    private String cifAccount;

    @JacksonXmlProperty(localName = "NextDate")
    @Schema(example = "2023-11-28")
    private String nextDate;

    @Data
    @JsonInclude(NON_EMPTY)
    private static class CreditProgramName {

      @JacksonXmlProperty(localName = "nil", namespace = "xsi", isAttribute = true)
      @Schema(example = "true")
      private String nil;
    }
  }

  @Data
  @JsonInclude(NON_EMPTY)
  public static class CreditCardList {

    @JacksonXmlProperty(localName = "gid", isAttribute = true)
    @Schema(example = "1004")
    private String gid;

    @JacksonXmlProperty(localName = "CreditCard")
    private CreditCard creditCard;

    @Data
    @JsonInclude(NON_EMPTY)
    public static class CreditCard {

      @JacksonXmlProperty(localName = "gid", isAttribute = true)
      @Schema(example = "1")
      private String gid;

      @JacksonXmlProperty(localName = "Card")
      @Schema(example = "4578xxxxxxxx8124")
      private String card;

      @JacksonXmlProperty(localName = "Contract")
      @Schema(example = "KZ11914CP39816914835")
      private String contract;

      @JacksonXmlProperty(localName = "LoanContractCC")
      @JsonProperty("loanContractCC")
      @Schema(example = "КК/22-06760481-00-ДБЗ")
      private String loanContractCC;
    }
  }
}
