package kz.bereke.qpdemo.model.loandebtgetlist.bcon;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_EMPTY;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.v3.oas.annotations.media.Schema;
import kz.bereke.esb.general.model.basic.response.BasicResponseDTO;
import kz.bereke.esb.general.model.basic.response.BasicResponseDTOHeader;
import kz.bereke.qpdemo.model.loandebtgetlist.qp.LoanDebtGetListQpResponse.PersonLoanDebtListResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@JsonInclude(NON_EMPTY)
public class LoanDebtGetListBconResponse extends BasicResponseDTO {

  private RsParms rsParms;

  public LoanDebtGetListBconResponse(BasicResponseDTOHeader requestHeader) {
    super(requestHeader);
  }

  @Data
  @Schema(name = "LoanDebtGetListRsParms")
  @NoArgsConstructor
  @AllArgsConstructor
  @JsonInclude(NON_EMPTY)
  public static class RsParms {

    private PersonLoanDebtListResponse personLoanDebtListResponse;
  }
}
