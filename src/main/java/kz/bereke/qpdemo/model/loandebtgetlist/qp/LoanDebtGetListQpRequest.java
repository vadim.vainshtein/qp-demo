package kz.bereke.qpdemo.model.loandebtgetlist.qp;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import java.util.List;
import kz.bereke.esb.qp.general.model.basic.BasicQPRequestDTO;
import kz.bereke.esb.qp.general.model.basic.QpRequestData;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
public class LoanDebtGetListQpRequest extends
    BasicQPRequestDTO<LoanDebtGetListQpRequest.RequestData> {

  @Data
  @NoArgsConstructor
  @AllArgsConstructor
  public static class RequestData implements QpRequestData {

    @JacksonXmlElementWrapper(localName = "PersonLoanDebtListRequests")
    @JacksonXmlProperty(localName = "PersonLoanDebtListRequest")
    private List<PersonLoanDebtListRequest> personLoanDebtListRequests;
  }

  @Data
  public static class PersonLoanDebtListRequest {

    @JacksonXmlProperty(localName = "TypePayment")
    private String typePayment;

    @JacksonXmlProperty(localName = "RNN")
    private String rnn;
  }
}
