package kz.bereke.qpdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QpDemoApplication {

  public static void main(String[] args) {
    SpringApplication.run(QpDemoApplication.class, args);
  }
}
