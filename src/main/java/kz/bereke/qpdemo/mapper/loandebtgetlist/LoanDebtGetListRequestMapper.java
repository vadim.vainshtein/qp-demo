package kz.bereke.qpdemo.mapper.loandebtgetlist;

import kz.bereke.esb.general.service.DateTimeService;
import kz.bereke.esb.qp.general.mapper.BasicQpRequestMapper;
import kz.bereke.qpdemo.model.loandebtgetlist.bcon.LoanDebtGetListBconRequest;
import kz.bereke.qpdemo.model.loandebtgetlist.qp.LoanDebtGetListQpRequest;
import kz.bereke.qpdemo.model.loandebtgetlist.qp.LoanDebtGetListQpRequest.RequestData;
import org.springframework.stereotype.Component;

@Component
public class LoanDebtGetListRequestMapper extends BasicQpRequestMapper<
    LoanDebtGetListBconRequest, RequestData, LoanDebtGetListQpRequest> {

  public LoanDebtGetListRequestMapper(DateTimeService dateTimeService) {
    super(dateTimeService);
  }

  @Override
  protected LoanDebtGetListQpRequest createQpRequest() {
    return new LoanDebtGetListQpRequest();
  }

  @Override
  public LoanDebtGetListQpRequest map(LoanDebtGetListBconRequest bconRequest) {
    var qpRequest = super.map(bconRequest);
    qpRequest.setFilialCode(bconRequest.getRqParms().getFilialCode());
    return qpRequest;
  }

  @Override
  protected RequestData createRequestData(LoanDebtGetListBconRequest bconRequest) {
    var rqParms = bconRequest.getRqParms();
    return new RequestData(rqParms.getPersonLoanDebtListRequests());
  }
}
