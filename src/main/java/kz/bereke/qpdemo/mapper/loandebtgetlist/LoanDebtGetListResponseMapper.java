package kz.bereke.qpdemo.mapper.loandebtgetlist;

import kz.bereke.esb.general.model.basic.response.BasicResponseDTOHeader;
import kz.bereke.esb.general.service.DateTimeService;
import kz.bereke.esb.qp.general.mapper.BasicQpResponseMapper;
import kz.bereke.qpdemo.model.loandebtgetlist.bcon.LoanDebtGetListBconResponse;
import kz.bereke.qpdemo.model.loandebtgetlist.bcon.LoanDebtGetListBconResponse.RsParms;
import kz.bereke.qpdemo.model.loandebtgetlist.qp.LoanDebtGetListQpResponse;
import kz.bereke.qpdemo.model.loandebtgetlist.qp.LoanDebtGetListQpResponse.ResponseData;
import org.springframework.stereotype.Component;

@Component
public class LoanDebtGetListResponseMapper extends BasicQpResponseMapper
    <LoanDebtGetListBconResponse, ResponseData, LoanDebtGetListQpResponse> {

  public LoanDebtGetListResponseMapper(DateTimeService dateTimeService) {
    super(dateTimeService);
  }

  @Override
  protected LoanDebtGetListBconResponse createBconResponse(BasicResponseDTOHeader header,
      LoanDebtGetListQpResponse qpResponse) {
    var bconResponse = new LoanDebtGetListBconResponse(header);
    bconResponse.setRsParms(new RsParms(qpResponse.getResponseData()
        .getPersonLoanDebtListResponse()));

    return bconResponse;
  }
}
