package kz.bereke.qpdemo.service;

import kz.bereke.esb.qp.general.builder.impl.MqRoRequestBuilder;
import kz.bereke.esb.qp.general.service.BasicQpRequestHandler;
import kz.bereke.esb.qp.general.service.MQDataManager;
import kz.bereke.qpdemo.mapper.loandebtgetlist.LoanDebtGetListRequestMapper;
import kz.bereke.qpdemo.mapper.loandebtgetlist.LoanDebtGetListResponseMapper;
import kz.bereke.qpdemo.model.loandebtgetlist.bcon.LoanDebtGetListBconRequest;
import kz.bereke.qpdemo.model.loandebtgetlist.bcon.LoanDebtGetListBconResponse;
import kz.bereke.qpdemo.model.loandebtgetlist.qp.LoanDebtGetListQpRequest;
import kz.bereke.qpdemo.model.loandebtgetlist.qp.LoanDebtGetListQpResponse;
import org.springframework.stereotype.Service;

@Service
public class LoanDebtGetListRequestHandler extends
    BasicQpRequestHandler<LoanDebtGetListBconRequest, LoanDebtGetListBconResponse, LoanDebtGetListQpRequest, LoanDebtGetListQpResponse> {

  public LoanDebtGetListRequestHandler(
      MQDataManager mqDataManager,
      MqRoRequestBuilder mqRequestBuilder,
      LoanDebtGetListRequestMapper requestMapper,
      LoanDebtGetListResponseMapper responseMapper) {

    super(
        mqDataManager,
        mqRequestBuilder,
        requestMapper,
        responseMapper,
        LoanDebtGetListQpResponse.class);
  }
}
